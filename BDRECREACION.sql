-- MySQL dump 10.13  Distrib 8.0.27, for Win64 (x86_64)
--
-- Host: localhost    Database: test
-- ------------------------------------------------------
-- Server version	8.0.27

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `contrato`
--

DROP TABLE IF EXISTS `contrato`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `contrato` (
  `id_contrato` int NOT NULL AUTO_INCREMENT,
  `id_cotizacion` varchar(45) NOT NULL,
  `fecha_creacion` datetime NOT NULL,
  `fecha_delevento` datetime NOT NULL,
  `correo` varchar(45) DEFAULT NULL,
  `numero_identificacion` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id_contrato`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contrato`
--

LOCK TABLES `contrato` WRITE;
/*!40000 ALTER TABLE `contrato` DISABLE KEYS */;
INSERT INTO `contrato` VALUES (1,'3','2021-11-18 00:00:00','2021-11-18 00:00:00','LuisaFernanda@Gmail.com','1036758451'),(2,'2','2021-11-19 00:00:00','2021-11-24 00:00:00','Yizosorio333@gmail.com','1005929131'),(3,'1','2021-11-19 00:00:00','2021-12-03 00:00:00','nestorl.osorio@hotmail.com','1024505319'),(4,'4','2021-12-03 00:00:00','2021-12-05 00:00:00','KEVIN.ORTIZ@GMAIL.COM','1004857512'),(5,'5','2021-12-03 00:00:00','2021-12-06 00:00:00','CAROLINA23@HOTMAIL.COM','1049758742'),(6,'6','2021-12-06 00:00:00','2021-12-07 00:00:00','JUANITA25@GMAIL.COM','1006869742');
/*!40000 ALTER TABLE `contrato` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contrato_recreador`
--

DROP TABLE IF EXISTS `contrato_recreador`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `contrato_recreador` (
  `id_contrato` int NOT NULL AUTO_INCREMENT,
  `id_recreador` int NOT NULL,
  `fecha_creacion` datetime NOT NULL,
  PRIMARY KEY (`id_contrato`,`id_recreador`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contrato_recreador`
--

LOCK TABLES `contrato_recreador` WRITE;
/*!40000 ALTER TABLE `contrato_recreador` DISABLE KEYS */;
INSERT INTO `contrato_recreador` VALUES (1,9,'2021-11-18 05:00:00'),(1,10,'2021-11-18 05:00:00'),(1,11,'2021-11-18 05:00:00'),(1,12,'2021-11-18 05:00:00'),(2,11,'2021-11-19 05:00:00'),(3,10,'2021-11-19 05:00:00'),(3,13,'2021-11-19 05:00:00'),(4,9,'2021-12-03 05:00:00'),(4,10,'2021-12-03 05:00:00'),(5,9,'2021-12-03 05:00:00'),(5,11,'2021-12-03 05:00:00'),(6,11,'2021-12-06 05:00:00'),(6,12,'2021-12-06 05:00:00');
/*!40000 ALTER TABLE `contrato_recreador` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cotizacion`
--

DROP TABLE IF EXISTS `cotizacion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `cotizacion` (
  `id_cotizacion` int NOT NULL AUTO_INCREMENT,
  `primer_nombre` varchar(45) NOT NULL,
  `segundo_nombre` varchar(45) DEFAULT NULL,
  `primer_apellido` varchar(45) NOT NULL,
  `segundo_apellido` varchar(45) DEFAULT NULL,
  `tipo_identificacion` varchar(45) NOT NULL,
  `numero_identificacion` int NOT NULL,
  `direccion_evento` varchar(45) NOT NULL,
  `barrio` varchar(45) NOT NULL,
  `localidad` varchar(45) NOT NULL,
  `hora` time NOT NULL,
  `fecha` datetime NOT NULL,
  `numero_contacto` varchar(10) NOT NULL,
  `segundo_contacto` varchar(10) DEFAULT NULL,
  `homenajeado` varchar(45) NOT NULL,
  `edad` varchar(45) NOT NULL,
  `email` varchar(45) NOT NULL,
  `id_paquete` int NOT NULL,
  `horas_evento` int NOT NULL,
  `valor_total` bigint NOT NULL,
  `saldo` bigint NOT NULL,
  `anticipo` bigint NOT NULL,
  `asesor` varchar(45) NOT NULL,
  `indicaciones` varchar(45) NOT NULL,
  PRIMARY KEY (`id_cotizacion`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cotizacion`
--

LOCK TABLES `cotizacion` WRITE;
/*!40000 ALTER TABLE `cotizacion` DISABLE KEYS */;
INSERT INTO `cotizacion` VALUES (1,'NESTOR','LEONEL','OSORIO','TOVAR','1',1024505319,'TRANSVERSAL 52 # 70 16 SUR','SIERRA MORENA','CIUDAD BOLIVAR','14:00:00','2021-12-04 00:00:00','3186345860','0','ALEXA','25','nestorl.osorio@hotmail.com',1,5,250000,100000,150000,'Maria','LLevar musica'),(2,'YERSSI','YISETH','OSORIO','TOVAR','1',1005929131,'CALLE 73A #48 - 15 SUR','JERUSALEN','Ciudad Bolivar','10:30:00','2021-11-13 00:00:00','3042823757','0','Ivan Rojas','25','Yizosorio333@gmail.com',7,10,500000,250000,250000,'Maria','Llegar 30 min antes'),(3,'LUISA','FERNANDA','CASTRO','LOPEZ','1',1036758451,'DIAGONAL 40 # 41 61','LAS MERCEDES','CIUDAD BONITA','16:30:00','2021-11-19 00:00:00','3015957484','0','KARLA','21','LuisaFernanda@Gmail.com',4,4,220000,70000,150000,'MARIA','Llegar 30 min antes.'),(4,'KEVIN','ALEJANDRO','ORTIZ','BERNAL','1',1004857512,'CARRERA 85 A # 78-12 NORTE','BARRIO CEDRITOS','USAQUEN','15:20:00','2021-12-04 00:00:00','3056869757','0','Frank','2','KEVIN.ORTIZ@GMAIL.COM',6,4,300000,200000,100000,'Yiseth','Llegar 30 min antes.'),(5,'CAROLINA','','VARGAS','','1',1049758742,'CONJUNTO PAJARITO','COLINA CAMPESTRE','USAQUEN','17:30:00','2021-12-07 00:00:00','3045758495','0','ESTEBAN','2','CAROLINA23@HOTMAIL.COM',8,4,480000,400000,80000,'Yiseth','Llegar 30 min antes.'),(6,'MARIMAR','SHAINA','GUTIERREZ','BELTRAN','1',1006869742,'CALLE 85 A # 46 - 15 SUR','SIERRA MORENA','CIUDAD BOLIVAR','16:30:00','2021-12-08 00:00:00','3241575845','0','MAICOL','1','JUANITA25@GMAIL.COM',5,4,250000,100000,150000,'Maria','Llegar 30 min antes.');
/*!40000 ALTER TABLE `cotizacion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `detalle_paquete`
--

DROP TABLE IF EXISTS `detalle_paquete`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `detalle_paquete` (
  `id_detalle` int NOT NULL,
  `id_paquete` int DEFAULT NULL,
  `observacion` varchar(145) DEFAULT NULL,
  `estado` int NOT NULL,
  `fecha_creacion` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id_detalle`),
  KEY `fk_paquete_idx` (`id_paquete`),
  CONSTRAINT `fk_paquete` FOREIGN KEY (`id_paquete`) REFERENCES `paquete` (`id_paquete`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `detalle_paquete`
--

LOCK TABLES `detalle_paquete` WRITE;
/*!40000 ALTER TABLE `detalle_paquete` DISABLE KEYS */;
INSERT INTO `detalle_paquete` VALUES (1,1,'1 RECREADOR',2,'2021-10-25'),(2,1,'RECREACION DIRIGIDA',2,'2021-10-25'),(3,1,'PINTUCARITAS',2,'2021-10-25'),(4,1,'GLOBOFLEXIA 20 NIÑOS MAX',2,'2021-10-25'),(5,1,'1 SHOW PRINCIPAL (PAYASOS – TITERES)',2,'2021-10-25'),(6,1,'MUSICA FORMATO USB',2,'2021-10-25'),(7,1,'2  HORAS',2,'2021-10-25'),(8,2,'2 RECREADOR',2,'2021-10-25'),(9,2,'RECREACION DIRIGIDA',2,'2021-10-25'),(10,2,'PINTUCARITAS',2,'2021-10-25'),(11,2,'GLOBOFLEXIA 30 NIÑOS MAX',2,'2021-10-25'),(12,2,'SHOW TITERES',2,'2021-10-25'),(13,2,'SHOW PAYASOS',2,'2021-10-25'),(14,2,'MUSICA FORMATO USB',2,'2021-10-25'),(15,2,'4 HORAS',2,'2021-10-25'),(16,3,'2 RECREADOR',2,'2021-10-25'),(17,3,'RECREACION DIRIGIDA',2,'2021-10-25'),(18,3,'PINTUCARITAS',2,'2021-10-25'),(19,3,'GLOBOFLEXIA 20 NIÑOS MAX',2,'2021-10-25'),(20,3,'1 SHOW PRINCIPAL (PAYASOS – TITERES)',2,'2021-10-25'),(21,3,'RUMBA DIRIGIDA',2,'2021-10-25'),(22,3,'MUSICA FORMATO USB',2,'2021-10-25'),(23,3,'4 HORAS',2,'2021-10-25'),(24,4,'2 RECREADOR',2,'2021-10-25'),(25,4,'RECREACION DIRIGIDA',2,'2021-10-25'),(26,4,'PINTUCARITAS',2,'2021-10-25'),(27,4,'GLOBOFLEXIA O MANILLAS NEON 30 NIÑOS MAX',2,'2021-10-25'),(28,4,'SHOW TITERES',2,'2021-10-25'),(29,4,'SHOW PAYASOS',2,'2021-10-25'),(30,4,'CHIKY TK (2 LUCES AUDIO RITMICAS, 1 CAMARA DE HUMO, NIEVE, EXPLOCION DE PAPEL)',2,'2021-10-25'),(31,4,'MUSICA FORMATO USB',2,'2021-10-25'),(32,4,'4 HORAS',2,'2021-10-25'),(33,5,'2 RECREADOR',2,'2021-10-25'),(34,5,'RECREACION DIRIGIDA',2,'2021-10-25'),(35,5,'PINTUCARITAS',2,'2021-10-25'),(36,5,'ELEMENTOS ESPECIALES PARA LA RECREACION DE LOS ADULTOS',2,'2021-10-25'),(37,5,'SHOW PAYASOS',2,'2021-10-25'),(38,5,'MINI-TK (2 CAMARAS DE HUMO, 2 LUCES AUDIORITMICAS, EXPLOSION DE PAPEL, LLUVIA DE NIEVE)',2,'2021-10-25'),(39,5,'MUSICA FORMATO USB',2,'2021-10-25'),(40,5,'4 HORAS',2,'2021-10-25'),(41,6,'2 RECREADOR',2,'2021-10-25'),(42,6,'RECREACION DIRIGIDA',2,'2021-10-25'),(43,6,'PINTUCARITAS',2,'2021-10-25'),(44,6,'ELEMENTOS ESPECIALES PARA LA RECREACION DE LOS ADULTOS',2,'2021-10-25'),(45,6,'SHOW PAYASOS',2,'2021-10-25'),(46,6,'VIEJO-TK (2 CAMARAS DE HUMO, 2 LUCES AUDIORITMICAS, EXPLOSION DE PAPEL, LLUVIA DE NIEVE)',2,'2021-10-25'),(47,6,'1 CABINA AUTOPOTENCIADA CON MICROFONO',2,'2021-10-25'),(48,6,'MUSICA FORMATO USB',2,'2021-10-25'),(49,6,'4 HORAS',2,'2021-10-25'),(50,7,'1 DJ',2,'2021-10-25'),(51,7,'1 ANIMADOR',2,'2021-10-25'),(52,7,'SONIDO ( 4 LUCES, 2 CABINAS, 1 CAMARA DE HUMO, MICROFONO, MIXER O MEZCLADOR ESTRUCTURA, MONTAJE)',2,'2021-10-25'),(53,7,'MANILLAS, EXPLOSION DE PAPEL, LLUVIA DE NIEVE, GORROS CARNAVAL, COLLAR CARNAVAL',2,'2021-10-25'),(54,8,'2 RECREADOR',2,'2021-12-03'),(55,8,'1 PISCINA DE PELOTAS',2,'2021-12-03'),(56,8,'1 TRIANGULO (RAMPA)',2,'2021-12-03'),(57,8,'1 MEDIO TUNEL',2,'2021-12-03'),(58,8,'1 ESCALERA',2,'2021-12-03'),(59,8,'1 RODADERO PLASTICO',2,'2021-12-03'),(60,8,'2 MUÑECOS INFLABLES',2,'2021-12-03'),(61,8,'PISO DECORATIVO',2,'2021-12-03'),(62,8,'4 DADOS',2,'2021-12-03'),(63,8,'1 DONA GIGANTE',2,'2021-12-03'),(64,8,'2 BULTOS DE PELOTAS ',2,'2021-12-03'),(65,8,'PINTUCARITAS',2,'2021-12-03');
/*!40000 ALTER TABLE `detalle_paquete` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `paquete`
--

DROP TABLE IF EXISTS `paquete`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `paquete` (
  `id_paquete` int NOT NULL,
  `nombre` varchar(45) NOT NULL,
  `estado` int NOT NULL,
  `fecha_creacion` datetime DEFAULT NULL,
  `precio` int DEFAULT NULL,
  `fecha_modificacion` datetime DEFAULT NULL,
  `imagen` varchar(2500) DEFAULT NULL,
  PRIMARY KEY (`id_paquete`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `paquete`
--

LOCK TABLES `paquete` WRITE;
/*!40000 ALTER TABLE `paquete` DISABLE KEYS */;
INSERT INTO `paquete` VALUES (1,'PAQUETE MINI',2,'2021-10-25 00:00:00',120000,NULL,'https://img.wallpapersafari.com/desktop/1680/1050/58/15/Vr5y8f.jpg'),(2,'PAQUETE MEDIO',2,'2021-10-25 00:00:00',160000,NULL,'https://th.bing.com/th/id/R.90d5d4019c69e4da611f12c184f4b10c?rik=bFKcp5ogT%2fgRMA&pid=ImgRaw&r=0'),(3,'PAQUETE SUPER',2,'2021-10-25 00:00:00',170000,NULL,'https://ae01.alicdn.com/kf/H65da69536f01415cb8a6fc979f917f0dt.jpg?width=800&height=800&hash=1600'),(4,'PAQUETE CHIKY-TK',2,'2021-10-25 00:00:00',220000,NULL,'https://www.estarguapas.com/pics/2020/09/30/mmtx-globos-de-cumpleanos-18-anos-feliz-cumpleanos-decoracion-regalo-18-regalos-cumpleanos-mujer-oro-rosa-con-guirnalda-banner-de-cumpleanos-para-fies-111693-3.jpg'),(5,'BABY SHOWER',2,'2021-10-25 00:00:00',250000,NULL,'https://m.media-amazon.com/images/I/71RMeyfbO0L.jpg'),(6,'BABY SHOWER',2,'2021-10-25 00:00:00',300000,NULL,'https://4.bp.blogspot.com/-0myCbnXM6EA/V-7at33T7QI/AAAAAAAAD8U/RLX_DfsnDm8hWKI-8l1UK5rSURPYmuJCACLcB/s1600/baby%2Bshower%2Blove%2B1.png'),(7,'SONIDO PROFESIONAL',2,'2021-10-25 00:00:00',500000,NULL,'https://i.pinimg.com/550x/87/64/7c/87647c57ff3acfb1f4d1503aa09317f6.jpg'),(8,'PISTA DE OBSTACULOS',2,'2021-12-03 00:00:00',480000,NULL,'https://i.pinimg.com/564x/cb/d2/a5/cbd2a5ba5ea3a26030270e23d9276364.jpg');
/*!40000 ALTER TABLE `paquete` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `persona`
--

DROP TABLE IF EXISTS `persona`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `persona` (
  `id_persona` int NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) DEFAULT NULL,
  `apellido` varchar(45) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `telefono` varchar(45) DEFAULT NULL,
  `saldo` double DEFAULT NULL,
  PRIMARY KEY (`id_persona`),
  UNIQUE KEY `email_UNIQUE` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `persona`
--

LOCK TABLES `persona` WRITE;
/*!40000 ALTER TABLE `persona` DISABLE KEYS */;
INSERT INTO `persona` VALUES (3,'Yiseth','Tovar','Ytovar@gmail.com','56646667',200),(11,'Ana Maria','Perez','Aperez@gmail.com','1234-555',300);
/*!40000 ALTER TABLE `persona` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `portafolio`
--

DROP TABLE IF EXISTS `portafolio`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `portafolio` (
  `id_portafolio` int NOT NULL,
  `nombre_portafolio` varchar(45) DEFAULT NULL,
  `url_imagen` varchar(250) DEFAULT NULL,
  `fecha_creacion` datetime DEFAULT NULL,
  `fecha_modificacion` datetime DEFAULT NULL,
  PRIMARY KEY (`id_portafolio`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `portafolio`
--

LOCK TABLES `portafolio` WRITE;
/*!40000 ALTER TABLE `portafolio` DISABLE KEYS */;
INSERT INTO `portafolio` VALUES (1,'niñabonita','https://comohacerpara.com/imge/18966-altavoces.jpg','2021-10-29 00:00:00',NULL),(2,'niños','https://www.aaamoving-store.com/wp-content/uploads/2018/07/Goodbye-Party-With-Kids.jpg','2021-10-29 00:00:00',NULL),(3,NULL,'https://mobeliahome.com/wp-content/uploads/2019/04/photo-1416339426675-1f96fd81b653-1400x700.jpg','2021-10-29 00:00:00',NULL),(4,NULL,'https://i.pinimg.com/564x/96/9a/02/969a02439f18885958db1b65dc3af39d.jpg','2021-10-29 00:00:00',NULL),(5,NULL,'https://i.pinimg.com/564x/1e/ef/0a/1eef0a1624391be76ee6662ea81a8121.jpg','2021-10-29 00:00:00',NULL),(6,NULL,'https://i.pinimg.com/564x/c2/1d/9d/c21d9d080a75fe2606c0ddf853915fc7.jpg','2021-10-29 00:00:00',NULL),(7,NULL,'https://i.pinimg.com/originals/08/49/35/084935c533956e45cb84ae4161a3ecc7.jpg','2021-10-29 00:00:00',NULL),(8,NULL,'https://bloximages.chicago2.vip.townnews.com/journaltimes.com/content/tncms/assets/v3/editorial/1/eb/1eb99cd2-3130-5b02-8ad6-aeda0e79cd3a/5c7f2696518d5.image.jpg?resize=1200%2C1200','2021-10-29 00:00:00',NULL),(9,NULL,'https://i.pinimg.com/originals/d4/55/64/d45564d61429f8168ab1b7a8c31ee58f.jpg','2021-10-29 00:00:00',NULL);
/*!40000 ALTER TABLE `portafolio` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `recreador`
--

DROP TABLE IF EXISTS `recreador`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `recreador` (
  `id_recreador` int NOT NULL AUTO_INCREMENT,
  `nombre_recreador` varchar(45) DEFAULT NULL,
  `numero_documento` int DEFAULT NULL,
  `fecha_creacion` datetime DEFAULT NULL,
  `rango` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id_recreador`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `recreador`
--

LOCK TABLES `recreador` WRITE;
/*!40000 ALTER TABLE `recreador` DISABLE KEYS */;
INSERT INTO `recreador` VALUES (9,'Nestor Leonel Osorio Tovar',1024505319,'2021-11-16 00:00:00','Master'),(10,'Yerssi Yiseth Osorio Tovar',1005929131,'2021-11-16 00:00:00','Junior'),(11,'Julian Perez',124554455,'2021-05-05 00:00:00','Junior'),(12,'Liza Amaya',852555555,'2021-06-06 00:00:00','Senior'),(13,'Juan Perez',102461544,'2021-05-05 00:00:00','Coordinador');
/*!40000 ALTER TABLE `recreador` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rol`
--

DROP TABLE IF EXISTS `rol`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `rol` (
  `id_rol` int NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) DEFAULT NULL,
  `id_usuario` int DEFAULT NULL,
  PRIMARY KEY (`id_rol`),
  KEY `id_rol_usuario_idx` (`id_usuario`),
  CONSTRAINT `id_rol_usuario` FOREIGN KEY (`id_usuario`) REFERENCES `usuario` (`id_usuario`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rol`
--

LOCK TABLES `rol` WRITE;
/*!40000 ALTER TABLE `rol` DISABLE KEYS */;
INSERT INTO `rol` VALUES (1,'ROLE_ADMIN',1),(2,'ROLE_USER',1),(3,'ROLE_USER',2),(4,'ROLE_NES',1);
/*!40000 ALTER TABLE `rol` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuario`
--

DROP TABLE IF EXISTS `usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `usuario` (
  `id_usuario` int NOT NULL AUTO_INCREMENT,
  `username` varchar(45) DEFAULT NULL,
  `password` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`id_usuario`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuario`
--

LOCK TABLES `usuario` WRITE;
/*!40000 ALTER TABLE `usuario` DISABLE KEYS */;
INSERT INTO `usuario` VALUES (1,'admin','$2a$10$VfkG7p6U4Rrmr0dxA/s3iODdXSn3wdCyvcc.V4yeuI59J49SmObRS'),(2,'user','$2a$10$kpb.bG6ULu9cnnEtwvROUeQqrH0eZp1Oob9xROhyd.hOocc2KoNSi'),(3,'ogapicapiedra','$2a$10$KLFXcE9HJh3CH2zRFUAw/uuRYFDp4JZuY/VCu/ZdHgcRbi4KcVOMa');
/*!40000 ALTER TABLE `usuario` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-12-20 17:42:06
