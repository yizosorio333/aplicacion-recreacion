package mx.com.gm.web;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.stream.Collectors;
import lombok.extern.slf4j.Slf4j;
import mx.com.gm.domain.Contrato;
import mx.com.gm.domain.ContratoRecreador;
import mx.com.gm.domain.ContratoRecreadorPK;
import mx.com.gm.domain.Cotizacion;
import mx.com.gm.domain.Paquete;
import mx.com.gm.domain.Recreador;
import mx.com.gm.dto.CondicionDTO;
import mx.com.gm.dto.ContratoDTO;
import mx.com.gm.dto.CotizacionDTO;
import mx.com.gm.dto.ErrorDTO;
import mx.com.gm.dto.EstadoDTO;
import mx.com.gm.dto.PaqueteDTO;
import mx.com.gm.dto.RecreadorDTO;
import mx.com.gm.servicio.ContratoRecreadorService;
import mx.com.gm.servicio.ContratoService;
import mx.com.gm.servicio.CotizacionService;
import mx.com.gm.servicio.DetallePaqueteService;
import mx.com.gm.servicio.PaqueteService;
import mx.com.gm.servicio.PortafolioService;
import mx.com.gm.servicio.RecreadorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.modelmapper.ModelMapper;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.User;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
@Slf4j
public class ControladorCotizacion {

    @Autowired
    private PaqueteService paqueteService;

    @Autowired
    private DetallePaqueteService detallePaqueteService;

    @Autowired
    private PortafolioService portafolioService;

    @Autowired
    private CotizacionService cotizacionService;

    @Autowired
    private ContratoService contratoService;
    @Autowired
    private ContratoRecreadorService contratoRecreadorService;

    @Autowired
    private RecreadorService recreadorService;

    @PostMapping("/cotizacion")
    public String cotizacion(Model model) {

        try {
            List<Paquete> paquetes = paqueteService.listarPaquete();

            List<PaqueteDTO> paquetesDTO = new ArrayList<>();
            for (Paquete paqueteEntity : paquetes) {
                PaqueteDTO paqueteDTO = new PaqueteDTO();
                paqueteDTO.setIdPaquete(paqueteEntity.getIdPaquete());
                paqueteDTO.setNombre(paqueteEntity.getNombre());
                paqueteDTO.setPrecio(paqueteEntity.getPrecio());
                paqueteDTO.setImagen(paqueteEntity.getImagen());
                paquetesDTO.add(paqueteDTO);
            }
            CotizacionDTO cotizacion = new CotizacionDTO();
            model.addAttribute("paquetes", paquetesDTO);
            model.addAttribute("cotizacion", cotizacion);
            EstadoDTO estado = new EstadoDTO();
            List<ErrorDTO> error = new ArrayList<>();
            estado.setError(error);
            estado.setCodigo(0);
            model.addAttribute("estado", estado);

            return "cotizacion";
        } catch (Exception e) {
            System.out.println("error: " + e.getMessage());
            return "cotizacion";
        }

    }

    @GetMapping("/agregar")
    public String agregar(Cotizacion cotizacion) {
        return "modificar";
    }

    @PostMapping("/loginPagina")
    public String loginPagina(Model model, @AuthenticationPrincipal User user) {

        try {
            List<Paquete> paquetes = paqueteService.listarPaquete();

            List<PaqueteDTO> paquetesDTO = new ArrayList<>();
            for (Paquete paqueteEntity : paquetes) {
                PaqueteDTO paqueteDTO = new PaqueteDTO();
                paqueteDTO.setIdPaquete(paqueteEntity.getIdPaquete());
                paqueteDTO.setNombre(paqueteEntity.getNombre());
                paqueteDTO.setPrecio(paqueteEntity.getPrecio());
                paqueteDTO.setImagen(paqueteEntity.getImagen());
                paquetesDTO.add(paqueteDTO);
            }
            CotizacionDTO cotizacion = new CotizacionDTO();
            model.addAttribute("paquetes", paquetesDTO);
            model.addAttribute("cotizacion", cotizacion);
            EstadoDTO estado = new EstadoDTO();
            List<ErrorDTO> error = new ArrayList<>();
            estado.setError(error);
            estado.setCodigo(0);
            model.addAttribute("estado", estado);

            return "cotizacion";
        } catch (Exception e) {
            System.out.println("error: " + e.getMessage());
            return "cotizacion";
        }

    }

    @PostMapping("/buscarCotizacion")
    public String buscarCotizacion(Model model) {
        CondicionDTO condicion = new CondicionDTO();
        condicion.setPintarNumero(0);
        condicion.setPintarFecha(0);
        model.addAttribute("condicion", condicion);

        return "buscarCotizacion";
    }

    @PostMapping("/buscarContrato")
    public String buscarContrato(Model model) {
        CondicionDTO condicion = new CondicionDTO();
        condicion.setPintarFecha(0);
        model.addAttribute("condicion", condicion);

        return "buscarContrato";

    }

    @PostMapping("/buscarCotizacionPorNumero")
    public String buscarCotizacionPorNumero(Model model, String numeroIdentificacion) {

        List<Cotizacion> cotizacionesEntity = this.cotizacionService.encontrarCotizacionByIdentificacion(numeroIdentificacion);
        ModelMapper modelMapper = new ModelMapper();
        List<CotizacionDTO> listaCotizaciones
                = cotizacionesEntity
                        .stream()
                        .map(user -> modelMapper.map(user, CotizacionDTO.class))
                        .collect(Collectors.toList());

        model.addAttribute("cotizaciones", listaCotizaciones);

        return "buscarCotizacionPorNumero";
    }

    @PostMapping("/buscarCotizacionPorfecha")
    public String buscarCotizacionPorfecha(Model model, String fecha) {

        List<Cotizacion> cotizacionesEntity = this.cotizacionService.encontrarCotizacionByFecha(java.sql.Date.valueOf(fecha));
        ModelMapper modelMapper = new ModelMapper();
        List<CotizacionDTO> listaCotizaciones
                = cotizacionesEntity
                        .stream()
                        .map(user -> modelMapper.map(user, CotizacionDTO.class))
                        .collect(Collectors.toList());

        model.addAttribute("cotizaciones", listaCotizaciones);

        return "buscarCotizacionPorfecha";
    }

    @PostMapping("/pintarBuscarCotizacion")
    public String pintarBuscarCotizacion(Model model, String tipoBusqueda) {
        List<CotizacionDTO> listaCotizaciones = new ArrayList<>();
        model.addAttribute("cotizaciones", listaCotizaciones);
        switch (tipoBusqueda) {

            case "1":
                return "buscarCotizacionPorNumero";
            case "2":
                return "buscarCotizacionPorFecha";

            default:
                return "buscarCotizacion";
        }
    }

    @PostMapping("/formCrearContrato")
    public String formCrearContrato(Model model, CotizacionDTO cotizacion) {
        ContratoDTO contratoDTO = new ContratoDTO();
        contratoDTO.setIdCotizacion(cotizacion.getIdCotizacion());
        contratoDTO.setNumeroIdentificacion(cotizacion.getNumeroIdentificacion());
        contratoDTO.setEmail(cotizacion.getEmail());
        contratoDTO.setFechadelEvento(cotizacion.getFecha());
        List<RecreadorDTO> cuantosRecreadores = new ArrayList<>();
        for (int a = 1; a <= cotizacion.getCantidadRecreadores(); a++) {
            RecreadorDTO recreador = new RecreadorDTO();
            recreador.setIdRecreador(new Long("" + a));
            cuantosRecreadores.add(recreador);
        }
        contratoDTO.setRecreadores(cuantosRecreadores);
        Calendar cal = Calendar.getInstance();
        java.util.Date calDate = cal.getTime();
        java.sql.Date sqlDate = new java.sql.Date(calDate.getTime());
        contratoDTO.setFechaCreacion(sqlDate);
        ModelMapper modelMapper = new ModelMapper();
        List<Recreador> recreadoresEntity = recreadorService.listarRecreadores();
        List<RecreadorDTO> recreadores = recreadoresEntity
                .stream()
                .map(recrea -> modelMapper.map(recrea, RecreadorDTO.class))
                .collect(Collectors.toList());

        model.addAttribute("recreadores", recreadores);
        model.addAttribute("contrato", contratoDTO);
        return "crearContrato";
    }

    @PostMapping("/guardarContrato")
    public String guardarContrato(Model model, ContratoDTO contrato) {
        System.out.println("");
        Contrato contratoEntity = new Contrato();
        contratoEntity.setIdCotizacion(contrato.getIdCotizacion());
        contratoEntity.setNumeroIdentificacion(contrato.getNumeroIdentificacion());
        contratoEntity.setCorreo(contrato.getEmail());
        contratoEntity.setFechaCreacion(contrato.getFechaCreacion());
        contratoEntity.setFechadelEvento(contrato.getFechadelEvento());
        contratoService.guardar(contratoEntity);
        Integer idcontrato = contratoService.buscarContratoByIdCotizacion(contrato.getIdCotizacion());
        log.info("Se realizo insert de contrato Nro: " + idcontrato);
        for (Integer idrec : contrato.getIdRecreador()) {
            ContratoRecreador contratoRecreeador = new ContratoRecreador();
            ContratoRecreadorPK pk = new ContratoRecreadorPK();
            pk.setIdContrato(idcontrato);
            pk.setIdRecreador(idrec);
            contratoRecreeador.setContratoRecreadorPK(pk);
            contratoRecreeador.setFechaCreacion(contrato.getFechaCreacion());
            contratoRecreadorService.guardar(contratoRecreeador);
            log.info("Se realizo insert del recreador exitos");
        }

        log.info("Se realizo insert exitos");
        model.addAttribute("contrato", contrato);

        return "crearContrato";
    }

    @PostMapping("/pintarBuscarContrato")
    public String pintarBuscarContrato(Model model, String tipoBusqueda) {
        List<ContratoDTO> listaContratoDTO = new ArrayList<>();
        model.addAttribute("contratos", listaContratoDTO);
        switch (tipoBusqueda) {

            case "1":
                return "buscarContratoNumero";
            case "2":
                return "buscarContratoFecha";

            default:
                return "buscarContrato";
        }
    }

    @GetMapping("/buscarContratoFechaPeticion")
    public String buscarContratoFecha(Model model, String fecha) {

        List<Contrato> contratosEntity = this.contratoService.buscarContratoByFechadelEvento(java.sql.Date.valueOf(fecha));

        ModelMapper modelMapper = new ModelMapper();
        List<ContratoDTO> listaContratos
                = contratosEntity
                        .stream()
                        .map(user -> modelMapper.map(user, ContratoDTO.class))
                        .collect(Collectors.toList()); 
        List<ContratoDTO> listaContratosCompletos = new ArrayList<>();
        for (ContratoDTO dto : listaContratos) {
            Cotizacion cotizacion = cotizacionService.encontrarCotizacion(dto.getIdCotizacion());
            dto.setNumeroContacto(cotizacion.getNumeroContacto());
            dto.setValorTotal(cotizacion.getValorTotal());
            listaContratosCompletos.add(dto);
        }

        model.addAttribute("contratos", listaContratosCompletos);

        return "buscarContratoFecha";
    }

    @PostMapping("/buscarContratoNumero") 
    public String buscarContratoNumero(Model model, String numeroIdentificacion) {
        try {
            List<Contrato> contratosEntity = this.contratoService.encontrarContratoByIdentificacion(numeroIdentificacion);
            ModelMapper modelMapper = new ModelMapper();
            List<ContratoDTO> listaContratos
                    = contratosEntity
                            .stream()
                            .map(user -> modelMapper.map(user, ContratoDTO.class))
                            .collect(Collectors.toList());

            List<ContratoDTO> listaContratosCompletos = new ArrayList<>();
            for (ContratoDTO dto : listaContratos) {
                Cotizacion cotizacion = cotizacionService.encontrarCotizacion(dto.getIdCotizacion());
                dto.setNumeroContacto(cotizacion.getNumeroContacto());
                dto.setValorTotal(cotizacion.getValorTotal());
                listaContratosCompletos.add(dto);
            }

            model.addAttribute("contratos", listaContratosCompletos);

            return "buscarContratoNumero";
        } catch (Exception e) {
            System.out.println("error: " + e.getMessage());
            return "errores";
        }
    }
}
