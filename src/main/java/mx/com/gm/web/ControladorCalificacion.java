package mx.com.gm.web;

import java.util.List;
import java.util.Properties;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import lombok.extern.slf4j.Slf4j;
import lombok.var;
import mx.com.gm.domain.Contrato;
import mx.com.gm.domain.EmailMessage;
import mx.com.gm.dto.CalificacionDTO;
import mx.com.gm.dto.ContratoDTO;
import mx.com.gm.dto.CotizacionDTO;
import mx.com.gm.servicio.ContratoService;
import mx.com.gm.servicio.EmailService;
import mx.com.gm.servicio.EmailServiceImpl;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.annotation.Bean;
import org.springframework.context.event.EventListener;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@Slf4j
public class ControladorCalificacion {
    @Autowired
    private final EmailService emailService;
    
    public ControladorCalificacion(EmailService emailService){
        this.emailService = emailService;
    }

    @Autowired
    private ContratoService contratoService;

    @Autowired
    private JavaMailSender mailSender;

    private Session session;

    @PostMapping("/califica")
    public String califica(Model model) {
        System.out.println("");
        model.addAttribute("mensajeError", "");
        return "califica";
    }

    @PostMapping("/calificaEvento")
    public String calificaEvento(Model model, CalificacionDTO calificacion) {
        System.out.println("");
        Contrato contratoEntity = contratoService.encontrarContrato(calificacion.getIdContrato());
        if (contratoEntity.getNumeroIdentificacion().equals(calificacion.getNumeroIdentificacion())) {
            System.out.println("Hemos enviado un mensaje de verificacion a su correo electronico.");
            model.addAttribute("calificacion", calificacion);
            model.addAttribute("calificacion", contratoEntity);
            int random = 0;
            Random r = new Random();
            int number = r.nextInt(10000);
            System.out.println("el randow es: " + number);
//            getJavaMailSender(contratoEntity.getCorreo(), number);

            //aqui enviar correo
            return "codigoVerificacion";
        } else {
            System.out.println("Usted no puede calificar este evento");
            model.addAttribute("mensajeError", "Usted no puede calificar este evento");
        }
        return "califica";
    }
//    @EventListener(ApplicationReadyEvent.class)
//    public void sendEmailMessage() {
//        emailSerivice.sendMessage("yizosorio333@gmail.com", "Bienvenido", "Espero estes bien.");
//
//    }

    @PostMapping("/send-email")
    public ResponseEntity sendEmail(@RequestBody EmailMessage emailMessage){
        this.emailService.sendTextEmail(emailMessage.getTo(),
                                        emailMessage.getSubject(),
                                        emailMessage.getMessage()
                );
        return ResponseEntity.ok("Success");
    }
 
    
    
    
    
    
    
//     public void sendEmail (){
//         SimpleMailMessage email = new SimpleMailMessage();
//
//            //recorremos la lista y enviamos a cada cliente el mismo correo
//            email.setTo(c.getEmail());
//            email.setSubject(subject);
//            email.setText(content);
//
//            mailSender.send(email);
//        }
//     }
//    private void enviarcorreo(String correo, Integer codigo) {
//        Properties properties = new Properties();
//        properties.put("mail.smtp.host", "mail.gmail.com");
//        properties.put("mail.smtp.starttls.enable", "true");
//        properties.put("mail.smtp.port", 465); 
//        properties.put("mail.smtp.mail.sender", "nestorl.osorio22@gmail.com");
//        session = Session.getDefaultInstance(properties);
//        try {
//            MimeMessage message = new MimeMessage(session);
//            message.setFrom(new InternetAddress("nestorl.osorio22@gmail.com"));
//            message.addRecipient(Message.RecipientType.TO, new InternetAddress("yizosorio333@gmail.com"));
//            message.setSubject("Prueba");
//            message.setText("Texto " + codigo);
//            Transport t = session.getTransport("smtp"); 
//            t.connect("nestorl.osorio22@gmail.com", "ingenio1024");
//            t.sendMessage(message, message.getAllRecipients());
//            t.close();
//        } catch (MessagingException me) {
//            System.out.println("errorsito: " + me);
//        } catch (Exception me) {
//            System.out.println("errorsito: " + me);
//        }
//
//    }
}
//    @Bean
//    public JavaMailSender getJavaMailSender(String correo, int number) {
//        JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
//        mailSender.setHost("smtp.gmail.com");
//        mailSender.setPort(587);
//
//        mailSender.setUsername("yizosorio333@gmail.com");
//        mailSender.setPassword("password");
//
//        Properties props = mailSender.getJavaMailProperties();
//        props.put("mail.transport.protocol", "smtp");
//        props.put("mail.smtp.auth", "true");
//        props.put("mail.smtp.starttls.enable", "true");
//        props.put("mail.debug", "true");
//
//        return mailSender;
//    }
