package mx.com.gm.web;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.stream.Collectors;
import lombok.extern.slf4j.Slf4j;
import mx.com.gm.domain.Contrato;
import mx.com.gm.domain.ContratoRecreador;
import mx.com.gm.domain.Cotizacion;
import mx.com.gm.domain.DetallePaquete;
import mx.com.gm.domain.Paquete;
import mx.com.gm.domain.Portafolio;
import mx.com.gm.domain.Recreador;
import mx.com.gm.dto.ContratoDTO;
import mx.com.gm.dto.CotizacionDTO;
import mx.com.gm.dto.DetallePaqueteDTO;
import mx.com.gm.dto.ErrorDTO;
import mx.com.gm.dto.EstadoDTO;
import mx.com.gm.dto.PaqueteDTO;
import mx.com.gm.dto.PortafolioDTO;
import mx.com.gm.dto.RecreadorDTO;
import mx.com.gm.servicio.ContratoRecreadorService;
import mx.com.gm.servicio.ContratoService;
import mx.com.gm.servicio.CotizacionService;
import mx.com.gm.servicio.DetallePaqueteService;
import mx.com.gm.servicio.PaqueteService;
import mx.com.gm.servicio.PortafolioService;
import mx.com.gm.servicio.RecreadorService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
@Slf4j
public class ControladorInicio {

    @Autowired
    private PaqueteService paqueteService;

    @Autowired
    private ContratoService contratoService;

    @Autowired
    private CotizacionService cotizacionService;

    @Autowired
    private DetallePaqueteService detallePaqueteService;

    @Autowired
    private PortafolioService portafolioService;

    @Autowired
    private RecreadorService recreadorService;

    @Autowired
    private ContratoRecreadorService contratoRecreadorService;

    @GetMapping("/")
    public String inicio(Model model, @AuthenticationPrincipal User user) {

        List<Paquete> paquetes = paqueteService.listarPaquete();
        List<DetallePaquete> detallesPaquete = new ArrayList<>();
        List<PortafolioDTO> portafoliosDTO = new ArrayList<>();

        List<PaqueteDTO> paquetesDTO = new ArrayList<>();
        for (Paquete paqueteEntity : paquetes) {
            PaqueteDTO paqueteDTO = new PaqueteDTO();
            paqueteDTO.setIdPaquete(paqueteEntity.getIdPaquete());
            paqueteDTO.setNombre(paqueteEntity.getNombre());
            List<DetallePaqueteDTO> detallePaqueteDTO = new ArrayList<>();
            detallesPaquete = detallePaqueteService.listarDetallePaqueteByIdPaquete(paqueteEntity.getIdPaquete());
            for (DetallePaquete detalle : detallesPaquete) {
                DetallePaqueteDTO detalleDTO = new DetallePaqueteDTO(detalle.getIdDetalle(), detalle.getObservacion());
                detallePaqueteDTO.add(detalleDTO);
            }
            paqueteDTO.setDetalle(detallePaqueteDTO);
            paqueteDTO.setPrecio(paqueteEntity.getPrecio());
            paqueteDTO.setImagen(paqueteEntity.getImagen());
            paquetesDTO.add(paqueteDTO);
        }

        List<Portafolio> portafolios = portafolioService.listarPortafolio();

        for (Portafolio portafolioEntity : portafolios) {
            PortafolioDTO portafolioDTO = new PortafolioDTO();
            portafolioDTO.setIdPortafolio(portafolioEntity.getIdPortafolio());
            portafolioDTO.setNombrePortafolio(portafolioEntity.getNombrePortafolio());
            portafolioDTO.setUrlImagen(portafolioEntity.getUrlImagen());
            portafolioDTO.setFechaCreacion(portafolioEntity.getFechaCreacion());
            portafolioDTO.setFechaModificacion(portafolioEntity.getFechaModificacion());
            portafoliosDTO.add(portafolioDTO);
        }

        log.info("ejecutando el controlador Spring MVC");
        log.info("usuario que hizo login:" + user);
        model.addAttribute("paquetes", paquetesDTO);
        model.addAttribute("portafolios", portafoliosDTO);

        return "index";

    }

    @PostMapping("/guardar")
    public String guardar(Model model, CotizacionDTO cotizacion) {
        try {
            Cotizacion cotizacionEntity = new Cotizacion();
            EstadoDTO estado = validarCotizacion(cotizacion);
            if (estado.getCodigo() == 1) {
                //tiene errores de campo 
                model.addAttribute("cotizacion", cotizacion);
                model.addAttribute("estado", estado);
                return "cotizacion";
            }
            cotizacionEntity.setPrimerNombre(cotizacion.getPrimerNombre());
            cotizacionEntity.setSegundoNombre(cotizacion.getSegundoNombre());
            cotizacionEntity.setPrimerApellido(cotizacion.getPrimerApellido());
            cotizacionEntity.setSegundoApellido(cotizacion.getSegundoApellido());
            cotizacionEntity.setTipoIdentificacion(cotizacion.getTipoIdentificacion());
            cotizacionEntity.setNumeroIdentificacion(cotizacion.getNumeroIdentificacion());
            cotizacionEntity.setDireccionEvento(cotizacion.getDireccionEvento());
            cotizacionEntity.setBarrio(cotizacion.getBarrio());
            cotizacionEntity.setLocalidad(cotizacion.getLocalidad());
            cotizacionEntity.setHora(cotizacion.getHora());
            cotizacionEntity.setFecha(cotizacion.getFecha());
            cotizacionEntity.setNumeroContacto(cotizacion.getNumeroContacto());
            cotizacionEntity.setSegundoContacto(cotizacion.getSegundoContacto());
            cotizacionEntity.setHomenajeado(cotizacion.getHomenajeado());
            cotizacionEntity.setEdad(cotizacion.getEdad());
            cotizacionEntity.setEmail(cotizacion.getEmail());
            cotizacionEntity.setIdPaquete(cotizacion.getIdPaquete());
            cotizacionEntity.setHorasEvento(cotizacion.getHorasEvento());
            cotizacionEntity.setValorTotal(cotizacion.getValorTotal());
            cotizacionEntity.setSaldo(cotizacion.getSaldo());
            cotizacionEntity.setAnticipo(cotizacion.getAnticipo());
            cotizacionEntity.setAsesor(cotizacion.getAsesor());
            cotizacionEntity.setIndicaciones(cotizacion.getIndicaciones());
            cotizacionService.guardar(cotizacionEntity);
            Cotizacion cotizacionDB = cotizacionService.encontrarCotizacionByIdentificacion(cotizacionEntity);
            log.info("Se realizo insert exitos");
            cotizacion.setIdCotizacion(cotizacionDB.getIdCotizacion());
            model.addAttribute("cotizacion", cotizacion);
            model.addAttribute("estado", estado);

            return "cotizacion";
        } catch (Exception e) {
            log.info("Se presento error al crear la cotizacion");
            log.info(e.getMessage());
            log.info(e.getLocalizedMessage());

            return "403";
        }

    }

    private EstadoDTO validarCotizacion(CotizacionDTO cotizacion) {
        EstadoDTO estado = new EstadoDTO();
        estado.setCodigo(0);
        List<ErrorDTO> errores = new ArrayList<>();

        if (cotizacion.getPrimerNombre() != null && cotizacion.getPrimerNombre().length() < 3) {
            ErrorDTO error = new ErrorDTO();
            error.setCodigo("Primer Nombre");
            error.setMensaje("El primer nombre es un campo obligatorio");
            errores.add(error);
            estado.setCodigo(1);
        }
        if (cotizacion.getPrimerApellido() != null && cotizacion.getPrimerApellido().length() < 3) {
            ErrorDTO error = new ErrorDTO();
            error.setCodigo("Primer Apellido");
            error.setMensaje("El primer apellido es un campo obligatorio");
            errores.add(error);
            estado.setCodigo(1);
        }
        if (cotizacion.getTipoIdentificacion() == null) {
            ErrorDTO error = new ErrorDTO();
            error.setCodigo("error");
            error.setMensaje("El tipo de identificacion es un campo obligatorio");
            errores.add(error);
            estado.setCodigo(1);
        }

        if (cotizacion.getNumeroIdentificacion() != null && cotizacion.getNumeroIdentificacion().length() < 3) {
            ErrorDTO error = new ErrorDTO();
            error.setCodigo("error");
            error.setMensaje("El numero de identificacion es un campo obligatorio");
            errores.add(error);
            estado.setCodigo(1);
        }

        if (cotizacion.getDireccionEvento() != null && cotizacion.getDireccionEvento().length() < 3) {
            ErrorDTO error = new ErrorDTO();
            error.setCodigo("error");
            error.setMensaje("La direccion es un campo obligatorio");
            errores.add(error);
            estado.setCodigo(1);
        }
        if (cotizacion.getLocalidad() != null && cotizacion.getLocalidad().length() < 3) {
            ErrorDTO error = new ErrorDTO();
            error.setCodigo("error");
            error.setMensaje("La localidad es un campo obligatorio");
            errores.add(error);
            estado.setCodigo(1);
        }
        if (cotizacion.getBarrio() != null && cotizacion.getBarrio().length() < 3) {
            ErrorDTO error = new ErrorDTO();
            error.setCodigo("error");
            error.setMensaje("El barrio es un campo obligatorio");
            errores.add(error);
            estado.setCodigo(1);
        }
        if (cotizacion.getHora() != null && cotizacion.getHora().length() < 3) {
            ErrorDTO error = new ErrorDTO();
            error.setCodigo("error");
            error.setMensaje("La cantidad de horas que dura el evento es un campo obligatorio");
            errores.add(error);
            estado.setCodigo(1);
        }

        if (cotizacion.getNumeroContacto() != null && cotizacion.getNumeroContacto().length() < 7) {
            ErrorDTO error = new ErrorDTO();
            error.setCodigo("error");
            error.setMensaje("El primer numero de contacto es un campo obligatorio");
            errores.add(error);
            estado.setCodigo(1);
        }
        if (cotizacion.getHomenajeado() != null && cotizacion.getHomenajeado().length() < 3) {
            ErrorDTO error = new ErrorDTO();
            error.setCodigo("error");
            error.setMensaje("El nombre del homenajeadoes un campo obligatorio");
            errores.add(error);
            estado.setCodigo(1);
        }
        if (cotizacion.getEmail() != null && cotizacion.getEmail().length() < 5) {
            ErrorDTO error = new ErrorDTO();
            error.setCodigo("error");
            error.setMensaje("El email un campo obligatorio");
            errores.add(error);
            estado.setCodigo(1);
        }
        if (cotizacion.getValorTotal() != null && cotizacion.getValorTotal() < 10000) {
            ErrorDTO error = new ErrorDTO();
            error.setCodigo("error");
            error.setMensaje("El valor total del evento no puede ser vacio o inferior a $ 100.000");
            errores.add(error);
            estado.setCodigo(1);
        }
        if (cotizacion.getSaldo() != null && cotizacion.getSaldo() > cotizacion.getValorTotal()) {
            ErrorDTO error = new ErrorDTO();
            error.setCodigo("error");
            error.setMensaje("El saldo a pagar del evento no puede ser mayor al valor total");
            errores.add(error);
            estado.setCodigo(1);
        }
        if (cotizacion.getAnticipo() != null && cotizacion.getAnticipo() > cotizacion.getValorTotal()) {
            ErrorDTO error = new ErrorDTO();
            error.setCodigo("error");
            error.setMensaje("El saldo a pagar del evento no puede ser mayor al valor total");
            errores.add(error);
            estado.setCodigo(1);
        }
        if (cotizacion.getNumeroContacto() != null && cotizacion.getNumeroContacto().length() > 10) {
            ErrorDTO error = new ErrorDTO();
            error.setCodigo("error");
            error.setMensaje("El Numero de contacto del evento no puede ser mayor a 10 digitos");
            errores.add(error);
            estado.setCodigo(1);
        }
        Calendar cal = Calendar.getInstance();
        java.util.Date calDate = cal.getTime();
        java.sql.Date sqlDate = new java.sql.Date(calDate.getTime());
        if (sqlDate.after(cotizacion.getFecha())) {
            ErrorDTO error = new ErrorDTO();
            error.setCodigo("error");
            error.setMensaje("La fecha del evento no puede ser inferior a la fecha de creacion");
            errores.add(error);
            estado.setCodigo(1);
        }

        estado.setError(errores);
        return estado;
    }

    @PostMapping("/loginadm")
    public String loginAdm(Model model, @AuthenticationPrincipal User user) {
        return "login2";
    }

    @PostMapping("/generarPDFContrato")
    public String generarPDFContrato(Model model, ContratoDTO contrato) {
        try {
            CotizacionDTO cotizacionDTO;
            Cotizacion cotizacion = cotizacionService.encontrarCotizacion(contrato.getIdCotizacion());
            ModelMapper modelMapper = new ModelMapper();
            cotizacionDTO = modelMapper.map(cotizacion, CotizacionDTO.class);

            Paquete paquete = new Paquete();
            paquete.setIdPaquete(cotizacion.getIdPaquete());
            Paquete paqueteEntity = paqueteService.encontrarPaquete(paquete);
            PaqueteDTO paqueteDTO = modelMapper.map(paqueteEntity, PaqueteDTO.class);

            List<DetallePaquete> detallesPaquete = new ArrayList<>();
            List<PaqueteDTO> paquetesDTO = new ArrayList<>();
            List<DetallePaqueteDTO> detallePaqueteDTO = new ArrayList<>();
            detallesPaquete = detallePaqueteService.listarDetallePaqueteByIdPaquete(paqueteEntity.getIdPaquete());
            for (DetallePaquete detalle : detallesPaquete) {
                DetallePaqueteDTO detalleDTO = new DetallePaqueteDTO(detalle.getIdDetalle(), detalle.getObservacion());
                detallePaqueteDTO.add(detalleDTO);
            }
            paqueteDTO.setDetalle(detallePaqueteDTO);

            List<ContratoRecreador> contratoRecreador = contratoRecreadorService.listarContratoPorIdContrato(contrato.getIdContrato());
            List<RecreadorDTO> recreadores = new ArrayList<>();
            for (ContratoRecreador contratoEntity : contratoRecreador) {
                Recreador recreador = recreadorService.encontrarRecreadorPorId(new Long("" + contratoEntity.getContratoRecreadorPK().getIdRecreador()));
                RecreadorDTO recreadorDTO = modelMapper.map(recreador, RecreadorDTO.class);
                recreadores.add(recreadorDTO);
            }

            model.addAttribute("recreadores", recreadores);
            model.addAttribute("paqueteDTO", paqueteDTO);
            model.addAttribute("cotizacionDTO", cotizacionDTO);
            model.addAttribute("contrato", contrato);
            return "contratoPDF";
        } catch (Exception e) {
            System.out.println("error: " + e.getMessage());
            return "errores";
        }

    }
}
