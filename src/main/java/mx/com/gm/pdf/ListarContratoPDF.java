package mx.com.gm.pdf;

import com.lowagie.text.Document;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import mx.com.gm.dto.ContratoDTO;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.web.servlet.view.document.AbstractPdfView;

@Component("/generarPDFContrato")
public class ListarContratoPDF extends AbstractPdfView {

    @Override
    protected void buildPdfDocument(Map<String, Object> map, Document dcmnt, PdfWriter writer, HttpServletRequest hsr, HttpServletResponse hsr1) throws Exception {
        @SuppressWarnings("unchecked")
        List<ContratoDTO> listaContratosCompletos = (List<ContratoDTO>) map.get("contratos");

        PdfPTable tablaContratos = new PdfPTable(5);
        listaContratosCompletos.forEach(contrato -> {
            tablaContratos.addCell(contrato.getIdContrato().toString());
            tablaContratos.addCell(contrato.getIdCotizacion().toString());
            tablaContratos.addCell(contrato.getFechadelEvento().toString());
            tablaContratos.addCell(contrato.getNumeroContacto());
            tablaContratos.addCell(contrato.getValorTotal().toString());

        });
        dcmnt.add(tablaContratos);
    }

}
