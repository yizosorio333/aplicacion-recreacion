package mx.com.gm.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.ToString;

@Data
@ToString
@AllArgsConstructor
public class PortafolioDTO {

    public PortafolioDTO() {

    }

    private Long idPortafolio;
    private String nombrePortafolio;
    private String urlImagen;
    private String fechaCreacion;
    private String fechaModificacion;

}
