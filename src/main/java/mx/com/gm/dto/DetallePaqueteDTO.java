/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.gm.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.ToString;

/**
 *
 * @author admin
 */
@Data
@ToString
@AllArgsConstructor
public class DetallePaqueteDTO {

    private Long idDetalle;
    private String observacion;
    //private Double precio;

}
