/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.gm.dto;

import java.util.List;

/**
 *
 * @author admin
 */
public class EstadoDTO {

    private int codigo;
    private List<ErrorDTO> error;

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public List<ErrorDTO> getError() {
        return error;
    }

    public void setError(List<ErrorDTO> error) {
        this.error = error;
    }

}
