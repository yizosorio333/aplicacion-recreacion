package mx.com.gm.dto;

import java.sql.Date;
import java.util.List;
import javax.validation.constraints.NotEmpty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.ToString;

@Data
@ToString
@AllArgsConstructor
public class ContratoDTO {

    public ContratoDTO() {

    }

    private Integer idContrato;
    private Integer idCotizacion;
    private String primerNombre;
    private String segundoNombre;
    private String primerApellido;
    private String segundoApellido;
    @NotEmpty
    private Date fechaCreacion;
    @NotEmpty
    private Date fechadelEvento;
    private String direccionEvento;
    private String numeroIdentificacion;
    private String numeroContacto;
    private Long valorTotal;
    private String email;
    private List<RecreadorDTO> recreadores;
    private List<Integer> idRecreador;

}
