/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.gm.dto;

/**
 *
 * @author admin
 */
public class CondicionDTO {

    private int pintarNumero;
    private int pintarFecha;

    public int getPintarNumero() {
        return pintarNumero;
    }

    public void setPintarNumero(int pintarNumero) {
        this.pintarNumero = pintarNumero;
    }

    public int getPintarFecha() {
        return pintarFecha;
    }

    public void setPintarFecha(int pintarFecha) {
        this.pintarFecha = pintarFecha;
    }

}
