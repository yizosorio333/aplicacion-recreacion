package mx.com.gm.dto;

import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.ToString;

@Data
@ToString
@AllArgsConstructor
public class RecreadorDTO {

    public RecreadorDTO() {

    }

    private Long idRecreador;
    private String nombreRecreador;
    private String numeroDocumento;
    private Date fechaCreacion;
    private String rango;

}
