/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.gm.dto;

import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.ToString;

/**
 *
 * @author admin
 */
@Data
@ToString
@AllArgsConstructor
public class PaqueteDTO {

    public PaqueteDTO() {
    }
    private Long idPaquete;
    private String nombre;
    private Integer estado;
    private String fecha_creacion;
    private String fecha_modificacion;
    private List<DetallePaqueteDTO> detalle;
    private Integer precio;
    private String imagen;

  
}
