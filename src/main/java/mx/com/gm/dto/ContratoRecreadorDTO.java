package mx.com.gm.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.ToString;

@Data
@ToString
@AllArgsConstructor
public class ContratoRecreadorDTO {

    public ContratoRecreadorDTO() {

    }

    private Integer idContrato;
    private int idRecreador;
    private String fechaCreacion;

}
