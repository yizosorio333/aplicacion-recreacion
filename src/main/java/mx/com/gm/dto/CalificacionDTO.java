package mx.com.gm.dto;

import lombok.Data;

@Data
public class CalificacionDTO {

    private Integer idContrato;

    private String numeroIdentificacion;
}
