package mx.com.gm.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.ToString;

@Data
@ToString
@AllArgsConstructor
public class DetallePortafolioDTO {
    
    private Long idPortafolio;
    private String urlImagen;
    
}
