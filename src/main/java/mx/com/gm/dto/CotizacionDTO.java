package mx.com.gm.dto;

import java.sql.Date;
import java.util.List;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.ToString;

@Data
@ToString
@AllArgsConstructor
public class CotizacionDTO {

    public CotizacionDTO() {
    }

    private Integer idCotizacion;
    @NotEmpty
    private String primerNombre;
    private String segundoNombre;
    @NotEmpty
    private String primerApellido;
    @NotEmpty
    private String segundoApellido;
    @NotEmpty
    private String tipoIdentificacion;
    @NotEmpty
    private String numeroIdentificacion;
    @NotEmpty
    private String direccionEvento;
    @NotEmpty
    private String barrio;
    @NotEmpty
    private String localidad;
    @NotEmpty
    private String hora;
    @NotEmpty
    private Date fecha;
    @NotEmpty
    private String numeroContacto;
    @NotEmpty
    private String segundoContacto;
    @NotEmpty
    private String homenajeado;
    @NotEmpty
    private String edad;
    @NotEmpty
    @Email
    private String email;
    @NotEmpty
    private Long idPaquete;
    @NotEmpty
    private Long horasEvento;
    @NotEmpty
    private Long valorTotal;
    @NotEmpty
    private Long saldo;
    @NotEmpty
    private Long anticipo;
    @NotEmpty
    private String asesor;
    @NotEmpty
    private String indicaciones;
    @NotEmpty
    private int cantidadRecreadores;

}
