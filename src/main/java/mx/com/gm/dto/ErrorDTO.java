package mx.com.gm.dto;
//import lombok.Builder;
//
//@Builder
public class ErrorDTO {

    public ErrorDTO() {
    }
    
    private String codigo;
    private String mensaje;

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

}
