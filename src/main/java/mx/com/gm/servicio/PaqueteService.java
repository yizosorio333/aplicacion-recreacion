package mx.com.gm.servicio;

import java.util.List;
import mx.com.gm.domain.Paquete;

public interface PaqueteService {

    public List<Paquete> listarPaquete();

    public void guardar(Paquete paquete);

    public void eliminar(Paquete paquete);

    public Paquete encontrarPaquete(Paquete paquete);
    
 

}
