package mx.com.gm.servicio;

import java.util.List;
import mx.com.gm.domain.DetallePaquete;

public interface DetallePaqueteService {

    public List<DetallePaquete> listarDetallePaquete();

    public void guardar(DetallePaquete detallePaquete);

    public void eliminar(DetallePaquete detallePaquete);

    public DetallePaquete encontrardetallePaquete(DetallePaquete detallePaquete);
    
    public List<DetallePaquete> listarDetallePaqueteByIdPaquete(Long idPaquete);
   
}
