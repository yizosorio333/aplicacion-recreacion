package mx.com.gm.servicio;

import java.util.List;
import mx.com.gm.dao.DetallePaqueteDao;
import mx.com.gm.domain.DetallePaquete;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class DetallePaqueteServiceImpl implements DetallePaqueteService {

    @Autowired
    private DetallePaqueteDao detallePaqueteDao;

    @Override
    @Transactional(readOnly = true)
    public List<DetallePaquete> listarDetallePaquete() {
        return (List<DetallePaquete>) detallePaqueteDao.findAll();
    }

    @Override
    @Transactional
    public void guardar(DetallePaquete detallePaquete) {
        detallePaqueteDao.save(detallePaquete);
    }

    @Override
    @Transactional
    public void eliminar(DetallePaquete detallePaquete) {
        detallePaqueteDao.delete(detallePaquete);
    }

    @Override
    @Transactional(readOnly = true)
    public DetallePaquete encontrardetallePaquete(DetallePaquete detallePaquete) {
        return detallePaqueteDao.findById(detallePaquete.getIdDetalle()).orElse(null);
    }

    @Override
    public List<DetallePaquete> listarDetallePaqueteByIdPaquete(Long idPaquete) {
        return detallePaqueteDao.findByIdPaquete(idPaquete);
    }

}
