package mx.com.gm.servicio;

import java.sql.Date;
import java.util.List;
import mx.com.gm.domain.Contrato;
import mx.com.gm.dto.ContratoDTO;

public interface ContratoService {

    public List<Contrato> listarContrato();

    public void guardar(Contrato contrato);

    public void eliminar(Contrato contrato);

    public Integer buscarContratoByIdCotizacion(Integer idcotizacion);

    public Integer buscarContratoByIdContrato(Integer idcontrato);

    public List<Contrato> encontrarContratoByIdentificacion(String numeroIdentificacion);

    public List<Contrato> buscarContratoByFechadelEvento(Date fechadelEvento);

    public Contrato encontrarContrato(Integer idContrato);

}
