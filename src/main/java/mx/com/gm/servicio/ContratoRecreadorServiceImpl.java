package mx.com.gm.servicio;

import java.util.List;
import mx.com.gm.dao.ContratoRecreadorDao;
import mx.com.gm.domain.ContratoRecreador;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ContratoRecreadorServiceImpl implements ContratoRecreadorService {

    @Autowired
    private ContratoRecreadorDao contratoRecreadorDao;

    @Override
    public List<ContratoRecreador> listarContrato() {
        return (List<ContratoRecreador>) contratoRecreadorDao.findAll();
    }

    @Override
    public void guardar(ContratoRecreador contratoRecreador) {
        contratoRecreadorDao.save(contratoRecreador);
    }

    @Override
    public void eliminar(ContratoRecreador contratoRecreador) {
        contratoRecreadorDao.delete(contratoRecreador);
    }

    @Override
    public List<ContratoRecreador> listarContratoPorIdContrato(int idContrato) {
        return contratoRecreadorDao.listarContratoPorIdContrato(idContrato);
    }

}
