package mx.com.gm.servicio;

import java.util.List;
import mx.com.gm.domain.Recreador;

public interface RecreadorService {

    public List<Recreador> listarRecreadores();

    public void guardar(Recreador recreador);

    public void eliminar(Recreador recreador);

    public Recreador encontrarRecreador(Recreador recreador);

    public Recreador encontrarRecreadorPorId(Long idRecreador);

}
