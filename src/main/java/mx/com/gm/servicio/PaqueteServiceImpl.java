package mx.com.gm.servicio;

import java.util.List;
import mx.com.gm.dao.PaqueteDao;
import mx.com.gm.domain.Paquete;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class PaqueteServiceImpl implements PaqueteService {

    @Autowired
    private PaqueteDao paqueteDao;

    @Override
    @Transactional(readOnly = true)
    public List<Paquete> listarPaquete() {
        return (List<Paquete>) paqueteDao.findAll();
    }

    @Override
    @Transactional
    public void guardar(Paquete paquete) {
        paqueteDao.save(paquete);
    }

    @Override
    @Transactional
    public void eliminar(Paquete paquete) {
        paqueteDao.delete(paquete);
    }

    @Override
    @Transactional(readOnly = true)
    public Paquete encontrarPaquete(Paquete paquete) {
        return paqueteDao.findById(paquete.getIdPaquete()).orElse(null);
    }

}
