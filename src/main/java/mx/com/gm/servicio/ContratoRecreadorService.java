package mx.com.gm.servicio;

import java.util.List;
import mx.com.gm.domain.ContratoRecreador;

public interface ContratoRecreadorService {

    public List<ContratoRecreador> listarContrato();

    public List<ContratoRecreador> listarContratoPorIdContrato(int idContrato);

    public void guardar(ContratoRecreador contratoRecreador);

    public void eliminar(ContratoRecreador contratoRecreador);

}
