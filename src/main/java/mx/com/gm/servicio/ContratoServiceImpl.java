package mx.com.gm.servicio;

import java.sql.Date;
import java.util.List;
import mx.com.gm.dao.ContratoDao;
import mx.com.gm.domain.Contrato;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class ContratoServiceImpl implements ContratoService {

    @Autowired
    private ContratoDao contratoDao;

    @Override
    @Transactional(readOnly = true)
    public List<Contrato> listarContrato() {
        return (List<Contrato>) contratoDao.findAll();
    }

    @Override
    @Transactional
    public void guardar(Contrato contrato) {
        contratoDao.save(contrato);
    }

    @Override
    @Transactional
    public void eliminar(Contrato contrato) {
        contratoDao.delete(contrato);
    }

    @Override
    public Integer buscarContratoByIdCotizacion(Integer idcotizacion) {
        Contrato retorno = contratoDao.buscarContratoByIdCotizacion(idcotizacion);
        return retorno.getIdContrato();
    }

    @Override
    public List<Contrato> buscarContratoByFechadelEvento(Date fechadelEvento) {
        return contratoDao.buscarContratoByFechadelEvento(fechadelEvento);
    }

    @Override
    public List<Contrato> encontrarContratoByIdentificacion(String numeroIdentificacion) {
        return contratoDao.findByIdentificacionLike(numeroIdentificacion);
    }

    @Override
    public Contrato encontrarContrato(Integer idContrato) {
        return contratoDao.findById(idContrato).orElse(null);
    }

    @Override
    public Integer buscarContratoByIdContrato(Integer idcontrato) {
        Contrato retorno = contratoDao.buscarContratoByIdContrato(idcontrato);
        return retorno.getIdContrato();
    }

}
