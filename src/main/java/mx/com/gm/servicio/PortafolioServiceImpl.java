package mx.com.gm.servicio;

import java.util.List;
import mx.com.gm.dao.PortafolioDao;
import mx.com.gm.domain.Portafolio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class PortafolioServiceImpl implements PortafolioService {

    @Autowired
    private PortafolioDao portafolioDao;

    @Override
    @Transactional(readOnly = true)
    public List<Portafolio> listarPortafolio() {
        return (List<Portafolio>) portafolioDao.findAll();
    }

    @Override
    @Transactional
    public void guardar(Portafolio portafolio) {
        portafolioDao.save(portafolio);
    }

    @Override
    @Transactional
    public void eliminar(Portafolio portafolio) {
        portafolioDao.delete(portafolio);
    }

    @Override
    @Transactional(readOnly = true)
    public Portafolio encontrarPortafolio(Portafolio portafolio) {
        return portafolioDao.findById(portafolio.getIdPortafolio()).orElse(null);
    }

}
