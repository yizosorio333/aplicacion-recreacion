package mx.com.gm.servicio;

import java.sql.Date;
import java.util.List;
import mx.com.gm.domain.Cotizacion;
import mx.com.gm.dto.CotizacionDTO;

public interface CotizacionService {
    public List<Cotizacion> listarCotizacion();

    public void guardar(Cotizacion cotizacion);

    public void eliminar(Cotizacion cotizacion);
    
    public Cotizacion encontrarCotizacion(Integer idCotizacion);

    public Cotizacion encontrarCotizacionByIdentificacion(Cotizacion cotizacion);
    
    public List<Cotizacion> encontrarCotizacionByIdentificacion(String numeroIdentificacion);
    
    public List<Cotizacion> encontrarCotizacionByFecha(Date fecha);

    
}
