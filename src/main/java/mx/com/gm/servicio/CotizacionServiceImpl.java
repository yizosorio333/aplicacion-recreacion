package mx.com.gm.servicio;

import java.sql.Date;
import java.util.List;
import mx.com.gm.dao.CotizacionDao;
import mx.com.gm.domain.Cotizacion;
import mx.com.gm.dto.CotizacionDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class CotizacionServiceImpl implements CotizacionService {

    @Autowired
    private CotizacionDao cotizacionDao;

    @Override
    @Transactional(readOnly = true)
    public List<Cotizacion> listarCotizacion() {
        return (List<Cotizacion>) cotizacionDao.findAll();
    }

    @Override
    @Transactional
    public void guardar(Cotizacion cotizacion) {
        cotizacionDao.save(cotizacion);
    }

    @Override
    @Transactional
    public void eliminar(Cotizacion cotizacion) {
        cotizacionDao.delete(cotizacion);
    }

    @Override
    @Transactional(readOnly = true)
    public Cotizacion encontrarCotizacionByIdentificacion(Cotizacion cotizacion) {
        return cotizacionDao.findByIdentificacion(cotizacion.getNumeroIdentificacion());
    }

    @Override
    @Transactional(readOnly = true)
    public List<Cotizacion> encontrarCotizacionByIdentificacion(String numeroIdentificacion) {
        return cotizacionDao.findByIdentificacionLike((numeroIdentificacion));
    }

    @Override
    public List<Cotizacion> encontrarCotizacionByFecha(Date fecha) {
        return cotizacionDao.findByFecha(fecha);
    }

    @Override
    @Transactional(readOnly = true)
    public Cotizacion encontrarCotizacion(Integer idCotizacion) {
        return cotizacionDao.findById(idCotizacion).orElse(null);
    }

}
