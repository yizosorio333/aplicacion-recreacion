package mx.com.gm.servicio;

import java.util.List;
import java.util.Optional;
import mx.com.gm.dao.RecreadorDao;
import mx.com.gm.domain.Recreador;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class RecreadorServiceImpl implements RecreadorService {

    @Autowired
    private RecreadorDao recreadorDao;

    @Override
    @Transactional(readOnly = true)
    public List<Recreador> listarRecreadores() {
        return (List<Recreador>) recreadorDao.findAll();
    }

    @Override
    @Transactional
    public void guardar(Recreador recreador) {
        recreadorDao.save(recreador);
    }

    @Override
    @Transactional
    public void eliminar(Recreador recreador) {
        recreadorDao.delete(recreador);
    }

    @Override
    @Transactional(readOnly = true)
    public Recreador encontrarRecreador(Recreador recreador) {
        return recreadorDao.findById(recreador.getIdRecreador()).orElse(null);
    }

    @Override
    public Recreador encontrarRecreadorPorId(Long idRecreador) {
        Optional<Recreador> recreador = recreadorDao.findById(idRecreador);
        return recreador.get();
    }

}
