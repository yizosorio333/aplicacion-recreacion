package mx.com.gm.servicio;

import java.util.List;
import mx.com.gm.domain.Portafolio;

public interface PortafolioService {

    public List<Portafolio> listarPortafolio();

    public void guardar(Portafolio portafolio);

    public void eliminar(Portafolio portafolio);

    public Portafolio encontrarPortafolio(Portafolio portafolio);


}
