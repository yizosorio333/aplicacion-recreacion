package mx.com.gm.dao;

import mx.com.gm.domain.Paquete;
import org.springframework.data.repository.CrudRepository;

public interface PaqueteDao extends CrudRepository<Paquete, Long> {

}
