package mx.com.gm.dao;

import java.sql.Date;
import java.util.List;
import org.springframework.data.repository.CrudRepository;
import mx.com.gm.domain.Contrato;
import mx.com.gm.dto.ContratoDTO;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface ContratoDao extends CrudRepository<Contrato, Integer> {

    @Query("select c from Contrato c where c.idCotizacion=:idCotizacion")
    public Contrato buscarContratoByIdCotizacion(@Param("idCotizacion") Integer idCotizacion);

    @Query("select c from Contrato c where c.idContrato=:idContrato")
    public Contrato buscarContratoByIdContrato(@Param("idContrato") Integer idContrato);

    @Query("select c from Contrato c where c.fechadelEvento=:fechadelEvento")
    public List<Contrato> buscarContratoByFechadelEvento(@Param("fechadelEvento") Date fechadelEvento);

    @Query("select c from Contrato c where c.numeroIdentificacion like %:numeroIdentificacion%")
    public List<Contrato> findByIdentificacionLike(@Param("numeroIdentificacion") String numeroIdentificacion);

}
