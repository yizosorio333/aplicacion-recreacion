package mx.com.gm.dao;

import java.util.List;
import mx.com.gm.domain.DetallePaquete;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

public interface DetallePaqueteDao extends CrudRepository<DetallePaquete, Long>{
    
    @Query("select c from DetallePaquete c where c.idPaquete=:idPaquete")
    public List<DetallePaquete> findByIdPaquete(@Param("idPaquete") Long idPaquete);
}
