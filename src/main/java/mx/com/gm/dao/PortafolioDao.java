package mx.com.gm.dao;

import mx.com.gm.domain.Portafolio;
import org.springframework.data.repository.CrudRepository;

public interface PortafolioDao extends CrudRepository<Portafolio, Long>{
    
}
