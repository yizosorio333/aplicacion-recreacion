package mx.com.gm.dao;

import java.util.List;
import mx.com.gm.domain.ContratoRecreador;
import mx.com.gm.domain.ContratoRecreadorPK;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

public interface ContratoRecreadorDao extends CrudRepository<ContratoRecreador, ContratoRecreadorPK> {

    @Query("select c from ContratoRecreador c where c.contratoRecreadorPK.idContrato=:idContrato")
    public List<ContratoRecreador> listarContratoPorIdContrato(@Param("idContrato") int idContrato);
 
}
