package mx.com.gm.dao;

import mx.com.gm.domain.Recreador;
import org.springframework.data.repository.CrudRepository;

public interface RecreadorDao extends CrudRepository<Recreador, Long> {

}
