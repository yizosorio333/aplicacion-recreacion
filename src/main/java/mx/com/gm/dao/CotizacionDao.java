package mx.com.gm.dao;

import java.sql.Date;
import java.util.List;
import mx.com.gm.domain.Cotizacion;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

public interface CotizacionDao extends CrudRepository<Cotizacion, Integer> {

    @Query("select c from Cotizacion c where c.numeroIdentificacion=:numeroIdentificacion")
    public Cotizacion findByIdentificacion(@Param("numeroIdentificacion") String numeroIdentificacion);

    @Query("select c from Cotizacion c where c.numeroIdentificacion like %:numeroIdentificacion%")
    public List<Cotizacion> findByIdentificacionLike(@Param("numeroIdentificacion") String numeroIdentificacion);
    
    @Query("select c from Cotizacion c where c.fecha >=:fecha")
    public List<Cotizacion> findByFecha(@Param("fecha") Date fecha);
}
