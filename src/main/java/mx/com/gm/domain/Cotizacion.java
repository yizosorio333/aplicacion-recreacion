package mx.com.gm.domain;

import java.io.Serializable;
import java.sql.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import lombok.Data;

@Data
@Entity
@Table(name = "cotizacion")
public class Cotizacion implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idCotizacion;

    @NotNull
    private String primerNombre;

    private String segundoNombre;

    @NotNull
    @NotEmpty
    private String primerApellido;


    private String segundoApellido;

    @NotNull
    @NotEmpty
    private String tipoIdentificacion;
    @NotNull

    private String numeroIdentificacion;
    @NotNull

    @NotNull
    @NotEmpty
    private String direccionEvento;

    @NotNull
    @NotEmpty
    private String barrio;

    @NotNull
    @NotEmpty
    private String localidad;

    @NotNull
    @NotEmpty
    private String hora;

    @NotNull
    private Date fecha;

    @NotNull
    @NotEmpty
    private String numeroContacto;

    @NotNull
    @NotEmpty
    private String segundoContacto;

    @NotNull
    @NotEmpty
    private String homenajeado;

    @NotNull
    @NotEmpty
    private String edad;

    @NotNull
    @Email
    private String email;

    @NotNull
    private Long idPaquete;

    @NotNull
    private Long horasEvento;

    @NotNull
    private Long valorTotal;

    @NotNull
    private Long saldo;

    @NotNull
    private Long anticipo;

    @NotNull
    @NotEmpty
    private String asesor;

    @NotNull
    private String indicaciones;

}
