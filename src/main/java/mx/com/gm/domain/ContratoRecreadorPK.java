/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.gm.domain;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author admin
 */
@Embeddable
public class ContratoRecreadorPK implements Serializable {

    @Basic(optional = false)
    private int idContrato;
    @Basic(optional = false)
    @NotNull
    private int idRecreador;

    public ContratoRecreadorPK() {
    }

    public ContratoRecreadorPK(int idContrato, int idRecreador) {
        this.idContrato = idContrato;
        this.idRecreador = idRecreador;
    }

    public int getIdContrato() {
        return idContrato;
    }

    public void setIdContrato(int idContrato) {
        this.idContrato = idContrato;
    }

    public int getIdRecreador() {
        return idRecreador;
    }

    public void setIdRecreador(int idRecreador) {
        this.idRecreador = idRecreador;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) idContrato;
        hash += (int) idRecreador;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ContratoRecreadorPK)) {
            return false;
        }
        ContratoRecreadorPK other = (ContratoRecreadorPK) object;
        if (this.idContrato != other.idContrato) {
            return false;
        }
        if (this.idRecreador != other.idRecreador) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.com.gm.domain.ContratoRecreadorPK[ idContrato=" + idContrato + ", idRecreador=" + idRecreador + " ]";
    }
    
}
