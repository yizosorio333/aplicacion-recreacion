package mx.com.gm.domain;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import lombok.Data;

@Data
@Entity
@Table(name = "paquete")
public class Paquete implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idPaquete;

    @NotEmpty
    private String nombre;

    @NotEmpty
    private Integer estado;

    @NotEmpty
    private String fecha_creacion;
    
    @NotNull
    private Integer precio;

    @NotEmpty
    private String fecha_modificacion;
    
    @NotEmpty
    private String imagen;
  
}
