/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.gm.domain;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author admin
 */
@Entity
@Table(name = "contrato_recreador")
@XmlRootElement

public class ContratoRecreador implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected ContratoRecreadorPK contratoRecreadorPK;
    @Basic(optional = false)
    @NotNull
    @Column(name = "fecha_creacion")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaCreacion;

    public ContratoRecreador() {
    }

    public ContratoRecreador(ContratoRecreadorPK contratoRecreadorPK) {
        this.contratoRecreadorPK = contratoRecreadorPK;
    }

    public ContratoRecreador(ContratoRecreadorPK contratoRecreadorPK, Date fechaCreacion) {
        this.contratoRecreadorPK = contratoRecreadorPK;
        this.fechaCreacion = fechaCreacion;
    }

    public ContratoRecreador(int idContrato, int idRecreador) {
        this.contratoRecreadorPK = new ContratoRecreadorPK(idContrato, idRecreador);
    }

    public ContratoRecreadorPK getContratoRecreadorPK() {
        return contratoRecreadorPK;
    }

    public void setContratoRecreadorPK(ContratoRecreadorPK contratoRecreadorPK) {
        this.contratoRecreadorPK = contratoRecreadorPK;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (contratoRecreadorPK != null ? contratoRecreadorPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ContratoRecreador)) {
            return false;
        }
        ContratoRecreador other = (ContratoRecreador) object;
        if ((this.contratoRecreadorPK == null && other.contratoRecreadorPK != null) || (this.contratoRecreadorPK != null && !this.contratoRecreadorPK.equals(other.contratoRecreadorPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.com.gm.domain.ContratoRecreador[ contratoRecreadorPK=" + contratoRecreadorPK + " ]";
    }
    
}
