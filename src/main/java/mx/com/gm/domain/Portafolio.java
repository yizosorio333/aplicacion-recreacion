package mx.com.gm.domain;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import lombok.Data;

@Data
@Entity
@Table(name = "portafolio")
public class Portafolio implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idPortafolio;

    @NotEmpty
    private String nombrePortafolio;

    @NotEmpty
    private String urlImagen;

    @NotEmpty
    private String fechaCreacion;

    @NotEmpty
    private String fechaModificacion;

}
